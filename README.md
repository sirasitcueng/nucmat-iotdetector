โครงงานนี้สร้างขึ้นโดย นาย ศิรสิทธิ์ ศรีใส

This project is made by Sirasit Sreesai.

## เว็บของโครงงานนี้
https://nucmat-iotdetector.web.app/#/

## โครงสร้างไฟล์
### nucmat-iotdetector-webapp

### src

| Folder | File Name | Description | 
| ------ | ------ | ------ |
| component | DataBar.js | component แสดงกล่องทั้ง 3 กล่อง |
|  | EachCard.js | component แสดงแต่ละกล่อง |
|  | Header.js | component แสดงส่วนหัวของทุกหน้า |
|  | LineChart.js | component แสดงกราฟเส้น |
|  | Table.js | component แสดงตาราง |
| database | firebase.js | เชื่อมต่อกับฐานข้อมูล |
| pages | Home.js | ดึงข้อมูลจากฐานข้อมูล |
|  | ShowData.js | รวมข้อมูลมาแสดงหน้าหลัก |
|  | About.js | แสดงข้อมูลหน้า /about |

 
### nucmat-iotdetector-microcontroller

IotReceiveData
- IotReceiveData.ino : ไฟล์หลัก อ่านสัญญาณจากรังสี
- App.h : เก็บฟังก์ชันต่าง ๆ ใช้บนไฟล์หลัก
- Button.h & Button.cpp : คลาสของปุ่มกด

IotSendData
- IotSendData.ino : ไฟล์หลัก ส่งข้อมูลไปยังฐานข้อมูล firebase (ใช้ Library Firebase_ESP_Client โดย K. Suwatchai (Mobizt))
- config.h : เก็บค่าต่าง ๆ
