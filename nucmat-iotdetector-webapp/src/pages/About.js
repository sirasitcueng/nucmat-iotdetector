import React from 'react';
import Header from '../component/Header';

const About = () => {
    return (
        <div>
            <Header />
            <div className="container-fluid">
                <div className="row">
                    <div className="col text-center">
                        <img src="/image1.jpg" height="250px"></img>
                    </div>
                    <div className="col text-center">
                        <img src="/image2.jpg" height="250px"></img>
                    </div>
                </div>
                
                <br/>
                <div className="myTitle">
                    เกี่ยวกับโครงงานนี้
                </div>
                <br />
                <div className="myNormalText">
                <t/>โครงงานนี้เป็นการพัฒนาเครื่องเฝ้าระวังรังสี (Radiation Area Monitoring) 
                ที่สามารถส่งข้อมูลไปยังเว็บเซิร์ฟเวอร์ผ่านไวไฟ (Wi-Fi) และระบบไอโอที (Internet of Thing, IoT) 
                เพื่อใช้งานในด้านการเฝ้าระวังรังสีในด้านความปลอดภัยมั่นคง (Safety and Security) 
                ลดความเสียหายจากการเกิดอุบัติเหตุจากรังสีโดยการวัดรังสีทางไกล และป้องกันการลักลอบนำวัสดุกัมมันตภาพรังสีออกจากห้องปฏิบัติการ 
                โครงงานนี้ใช้ลิเทียม-ไอออนแบตเตอรี่ (Lithium-Ion Battery) ที่สามารถชาร์จได้ จ่ายไฟฟ้าให้กับวงจร 
                ใช้หัววัดรังสีไกเกอร์มูลเลอร์ (Geiger-Muller) วัดรังสีโดยจ่ายไฟฟ้าแรงดันสูง 500 โวลต์ ผ่านวงจรแปลงสัญญาณเป็นดิจิตอล 
                สัญญาณดิจิตอลจะถูกนับวัดในไมโครคอนโทรลเลอร์ (Microcontroller) เบอร์ ESP32 จากนั้นข้อมูลจะถูกส่งเข้าเว็บเซิร์ฟเวอร์ผ่านไวไฟ (Wi-Fi) 
                และเก็บอยู่ในคลาวด์เก็บข้อมูล (Cloud Storage) ข้อมูลจะถูกแสดงผลผ่านหน้าเว็บ
                </div>
                <br />
                <div className="myTitle">
                    เกี่ยวกับผู้จัดทํา
                </div>
                <br />
                <div className="myNormalText">
                    <div className="row">
                        <div className="col-3 myBoldText">
                            ผู้จัดทํา
                        </div>
                        <div className="col-9">
                            นาย ศิรสิทธิ์ ศรีใส นิสิตชั้นปีที่ 4
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-3 myBoldText">
                            อาจารย์ที่ปรึกษา
                        </div>
                        <div className="col-9">
                            อ.เดโช ทองอร่าม
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-3 myBoldText">
                            เป็นส่วนหนึ่งของรายวิชา
                        </div>
                        <div className="col-9">
                            โครงงานด้านวิศวกรรมนิวเคลียร์ ภาควิชาวิศวกรรมนิวเคลียร์ คณะวิศวกรรมศาสตร์ จุฬาลงกรณ์มหาวิทยาลัย
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About;