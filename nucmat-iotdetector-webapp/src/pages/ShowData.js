import React, {Component} from "react";
import DataBar from "../component/DataBar";
import LineChart from "../component/LineChart";
import Table from "../component/Table";

class ShowData extends Component{

    constructor(props){
        super(props);
    }

    calAverageValue(){
        if (this.props.dataList){
            var sum = 0;
            this.props.dataList.forEach(data => {
                sum += data.radiationData;
            });
            return Math.round(sum/this.calNumberValue() * 100) / 100;
        }
    }

    calNumberValue(){
        if (this.props.dataList){
            return this.props.dataList.length;
        }
    }

    calStdValue(){
        if (this.props.dataList){
            var squareSum = 0;
            this.props.dataList.forEach(data => {
                squareSum += Math.pow(data.radiationData - this.calAverageValue(), 2);
            });
            return Math.round(Math.sqrt(squareSum/(this.calNumberValue() - 1)) * 100) / 100 ;

        }
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <DataBar dataList={this.props.dataList} 
                                averageValue={this.calAverageValue()} 
                                numberValue={this.calNumberValue()} 
                                stdValue={this.calStdValue()} />
                </div>
                <br />
                <hr />
                <div className="row">
                    <div className="col">
                        <Table dataList={this.props.dataList} 
                                warningLevel={this.calAverageValue() + this.calStdValue()} 
                                dangerLevel={this.calAverageValue() + 2 * this.calStdValue()}/>
                    </div>
                    <div className="col">
                        <div className="myTableHead">Data Chart</div>
                        <hr />
                        <LineChart dataList={this.props.dataList} />
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowData;