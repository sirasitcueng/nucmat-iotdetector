import React, {Component} from 'react';
import Header from "../component/Header";
import firebaseApp from '../database/firebase';
import ShowData from './ShowData';

class Home extends Component {

  constructor(props){
    super(props);
    this.state = {dataList : ""};
  }

  componentDidMount(){
    /*this.setState({dataList: [
      {dataId: 1, dateStamp: "1/3/2564", timeStamp: "13:00:00", radiationData: 100},
      {dataId: 2, dateStamp: "1/3/2564", timeStamp: "14:00:00", radiationData: 200},
      {dataId: 3, dateStamp: "1/3/2564", timeStamp: "15:00:00", radiationData: 400},
      {dataId: 4, dateStamp: "1/3/2564", timeStamp: "16:00:00", radiationData: 800},
      {dataId: 5, dateStamp: "1/3/2564", timeStamp: "17:00:00", radiationData: 1600},
      {dataId: 6, dateStamp: "1/3/2564", timeStamp: "18:00:00", radiationData: 3200},
    ]})*/
    this.queryData();
  }

  async queryData () {
    const firestore = firebaseApp.firestore();
    await firestore.collection('data').onSnapshot((snapshot) => {
      snapshot.forEach((doc) => {
        this.setState({
          dataList : [
            ...this.state.dataList, {
              dateStamp : doc.data().date,
              timeStamp : doc.data().time,
              radiationData : parseInt(doc.data().count),
            }
          ]
        })
      })
    })
  }
  

  render(){
    return (
      <div>
          <Header />
          <ShowData dataList={this.state.dataList} />
      </div>
    )
  }
}

export default Home;
