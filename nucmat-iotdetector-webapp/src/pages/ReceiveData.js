import React, {Component} from 'react';
import queryString from 'query-string';
import firebaseApp from '../database/firebase';

class ReceiveData extends Component {

    constructor(props){
        super(props);
        this.state = {
            date : "",
            time : "",
            count : "",
        }
    }

    async submitDataToFirebase(){
        const db = firebaseApp.firestore();
        
        return new Promise((resolve, reject) => {
            db.collection('data').add({
                date : this.getParams().date,
                time : this.getParams().time,
                count : this.getParams().count,
            });
            resolve(`success \ndate : ${this.getParams().date}\ntime : ${this.getParams().time}\ncount : ${this.getParams().count}`);
            reject(new Error("Error Found: "));
        })
        .then((result) => {
            alert(result);
        })
        .catch((error) => {
            alert(error);
        })
    }

    getParams(){
        let url = this.props.location.search;
        let params = queryString.parse(url);
        /*console.log(params)
        if (this.state.count === null){
            await this.setState ({
                date : params.date,
                time : params.time,
                count : params.count,
            })
        }*/
        return params;
    }

    componentDidMount(){
        this.submitDataToFirebase();
    }



    render(){
        
        return (
            <div>

            </div>
        )
    }
}

export default ReceiveData;