import React, {Component} from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import Home from './pages/Home'
import ReceiveData from './pages/ReceiveData'
import About from './pages/About';

class App extends Component {

  constructor(props){
    super(props);
  }

  renderRouter(){
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/receiveData" component={ReceiveData} />
      </Switch>
    )
  }
  

  render(){
    return (
      <BrowserRouter>
        <HashRouter basename="/">
          {this.renderRouter()}
        </HashRouter >
      </BrowserRouter>
    )
  }
}

export default App;
