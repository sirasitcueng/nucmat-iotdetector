import firebase from 'firebase/app';
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyAuPzaOzG9imj0ZqK-Lwwe8VqifsmYkxtk",
    authDomain: "nucmat-iotdetector.firebaseapp.com",
    projectId: "nucmat-iotdetector",
    storageBucket: "nucmat-iotdetector.appspot.com",
    messagingSenderId: "38277389003",
    appId: "1:38277389003:web:ca581705adfcc2d4d8194e",
    measurementId: "G-BSP2MPKHLG"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp;
