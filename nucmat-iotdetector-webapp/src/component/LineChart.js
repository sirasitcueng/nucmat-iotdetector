import React, {Component} from 'react';
import Chart from 'react-google-charts';

class LineChart extends Component{


    
    showChart(){
        if (this.props.dataList){
            if (this.props.dataList.length > 0){
                this.props.dataList.sort((x, y) => {
                    let xDate = parseInt(x.dateStamp.split(':').join(''));
                    let xTime = parseInt(x.timeStamp.split('/').join(''));
                    let yDate = parseInt(y.dateStamp.split(':').join(''));
                    let yTime = parseInt(y.timeStamp.split('/').join(''));
                    if (xDate > yDate){
                        return 1;
                    }
                    else if (xDate < yDate){
                        return -1;
                    }
                    else{
                        return (xTime > yTime) ? 1 : -1;
                    }
                });
                var dataArray = [["Time", "Count"]];
                this.props.dataList.forEach(data => {
                    dataArray.push([this.getDateTime(data.dateStamp, data.timeStamp), data.radiationData]);
                    //dataArray.push([data.dataId, data.radiationData]);
                });
                var dateArray = [];
                this.props.dataList.forEach(data => {
                    dateArray.push(this.getDateTime(data.dateStamp, data.timeStamp))
                })
                var maxDate = new Date(Math.max(...dateArray));
                var minDate = new Date(Math.min(...dateArray));
                maxDate.setHours(maxDate.getHours() + 1);
                minDate.setHours(minDate.getHours() - 1);
                return(
                    <div>
                        <Chart 
                            width="100%"
                            height="300px"
                            chartType="LineChart"
                            loader={<div>Loading Chart</div>}
                            data={dataArray}
                            options={{
                                pointSize: 10,
                                fontSize: 12,
                                hAxis: {
                                    viewWindow: {
                                        min: minDate,
                                        max : maxDate,
                                    }
                                },
                                chartArea: {
                                    height: '80%'
                                },
                                series: {
                                    0: { curveType: 'function' },
                                },
                            }}
                            
                        />
                    </div>
                )
            }
            else{
                return(
                    <div>
                        no data
                    </div>
                )
            }
        }
    }

    getDateTime(myDate, myTime){
        var dateArray = myDate.split("/").reverse();
        var timeArray = myTime.split(":");
        dateArray[0] -= 543;
        dateArray[1] -= 1;
        return new Date(dateArray[0], dateArray[1], dateArray[2], timeArray[0], timeArray[1], timeArray[2]);
    }


    render(){
        return(
            <div>
                {this.showChart()}
            </div>
        )
    }
}

export default LineChart;