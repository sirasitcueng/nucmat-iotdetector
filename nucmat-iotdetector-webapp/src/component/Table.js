import React, {Component} from "react";
import "../App.css"

class Table extends Component{

    buildTable(){
        if (this.props.dataList){
            let datalist = this.props.dataList;
            datalist.sort((x, y) => {
                let xDate = parseInt(x.dateStamp.split(':').join(''));
                let xTime = parseInt(x.timeStamp.split('/').join(''));
                let yDate = parseInt(y.dateStamp.split(':').join(''));
                let yTime = parseInt(y.timeStamp.split('/').join(''));
                if (xDate > yDate){
                    return 1;
                }
                else if (xDate < yDate){
                    return -1;
                }
                else{
                    return (xTime > yTime) ? 1 : -1;
                }
            });
            return datalist.map((data, index) => (
                <div className={this.getColorStatus(data.radiationData)} key={index}>
                    <div className="col text-center">{index + 1}</div>
                    <div className="col text-center">{data.dateStamp}</div>
                    <div className="col text-center">{data.timeStamp}</div>
                    <div className="col text-center">{data.radiationData}</div>
                </div>
            ))
        }
    }

    getColorStatus(value){
        return (value < this.props.warningLevel) ? "row" 
        : (value < this.props.dangerLevel) ? "row table-warning"
        : "row table-danger";
    }

    render(){
        return(
            <div className="container-fluid table">
                <div className="row">
                    <div className="col myTableHead">#</div>
                    <div className="col myTableHead">Date</div>
                    <div className="col myTableHead">Time</div>
                    <div className="col myTableHead">Count</div>
                </div>
                <hr />
                <div className="container-fluid col" style={{height: "300px", overflowY: "scroll"}}>
                    {this.buildTable()}
                </div>
                
            </div>
        );
    }
}

export default Table;