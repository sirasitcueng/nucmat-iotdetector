import React, {Component} from "react";

class EachCard extends Component{
    render(){
        return(
            <div className="card">
                <div className="card-header myCardHeader">{this.props.cardHeader}</div>
                <div className="card-body myCardBody">{this.props.cardBody}</div>
            </div>
        )
    }
}

export default EachCard;