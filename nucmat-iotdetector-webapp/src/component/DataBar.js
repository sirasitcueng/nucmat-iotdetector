import React, {Component} from "react";
import EachCard from "./EachCard";

class DataBar extends Component{

    constructor(props){
        super(props);
    }

    calAverageValue(){
        if (this.props.dataList){
            var sum = 0;
            this.props.dataList.forEach(data => {
                sum += data.radiationData;
            });
            return Math.round(sum/this.calNumberValue() * 100) / 100;
        }
    }

    calNumberValue(){
        if (this.props.dataList){
            return this.props.dataList.length;
        }
    }

    calStdValue(){
        if (this.props.dataList){
            var squareSum = 0;
            this.props.dataList.forEach(data => {
                squareSum += Math.pow(data.radiationData - this.calAverageValue(), 2);
            });
            return Math.round(Math.sqrt(squareSum/(this.calNumberValue() - 1)) * 100) / 100 ;

        }
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="card-deck">
                    <EachCard cardHeader="Number of Data (N)" cardBody={this.props.numberValue} />
                    <EachCard cardHeader="Average Value (X bar)" cardBody={this.props.averageValue} />
                    <EachCard cardHeader="Standard Deviation (S.D.)" cardBody={this.props.stdValue} />

                </div>
            </div>
        )
    }
}

export default DataBar;