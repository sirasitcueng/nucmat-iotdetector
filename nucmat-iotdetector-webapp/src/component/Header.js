import React, {Component} from "react";
import {Link} from "react-router-dom";

class Header extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="mySuperTitle">เครื่องเฝ้าระวังรังสี (Area Monitoring Detector)</div>
                <div className="text-right">
                    <ul className="list-inline myNormalText">
                        <li className="list-inline-item"><Link to="/">ข้อมูลแสดงผล</Link></li>
                        <li className="list-inline-item">|</li>
                        <li className="list-inline-item"><Link to="/about">เกี่ยวกับโครงงานนี้</Link></li>
                    </ul>
                </div>
                <hr />
            </div>
        );
    }

}

export default Header;