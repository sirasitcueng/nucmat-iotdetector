#ifndef CONFIG_H
#define CONFIG_H

#define WIFI_SSID ""
#define WIFI_PASSWORD ""

#define API_KEY ""

#define FIREBASE_PROJECT_ID ""

#define USER_EMAIL ""
#define USER_PASSWORD ""

#define DOCUMENT_PATH ""
#endif
