/**
 * Reference for firebase library "Firebase_ESP_Client"
 * 
 * Created by K. Suwatchai (Mobizt)
 * Email: k_suwatchai@hotmail.com
 * Github: https://github.com/mobizt
 * Copyright (c) 2021 mobizt
 * 
 * Thank you very much for your helpful library!
 * Sirasit Sreesai
 *
*/

#include "config.h"
#include <WiFi.h>
#include <Firebase_ESP_Client.h>
#include "addons/TokenHelper.h"

FirebaseData myData;
FirebaseAuth myAuth;
FirebaseConfig myConfig;

unsigned long myMillis = 0;

String dateString = "02/03/2564";
String timeString = "06:00:00";
int countInt = 15;

void setup(){
  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED);
  
  myConfig.api_key = API_KEY;
  myAuth.user.email = USER_EMAIL;
  myAuth.user.password = USER_PASSWORD;
  myConfig.token_status_callback = tokenStatusCallback;
  
  Firebase.begin(&myConfig, &myAuth);
  Firebase.reconnectWiFi(true);
}

void loop(){
  if (Firebase.ready() && (millis() - myMillis > 360000 || myMillis == 0)){
    myMillis = millis();
    String content;
    FirebaseJson js;
    js.set("fields/date/stringValue", String(dateString).c_str());
    js.set("fields/time/stringValue", String(timeString).c_str());
    js.set("fields/count/integerValue", countInt);
    js.toString(content);
    if (Firebase.Firestore.createDocument(&myData, FIREBASE_PROJECT_ID, "", DOCUMENT_PATH, content.c_str())){
      Serial.println(myData.payload());
      Serial.println();
    }
  }
}
