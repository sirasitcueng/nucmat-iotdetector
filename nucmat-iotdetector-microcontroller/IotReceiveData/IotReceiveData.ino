#include "Button.h"
#include "App.h"

STATE state;
int settingTime = 5;
volatile int t = 5;
unsigned long v = 0;

hw_timer_t *timer = NULL;

Button buttonTimeUp(pinTIMEUP);
Button buttonTimeDown(pinTIMEDOWN);
Button buttonCount(pinCOUNT);

void setup() {
  timerSetting();
  Serial.begin(9600);

  delay(3000);
  state = STATE_STARTCOUNT;
}

void loop() {

  switch (state) {
    
    case STATE_READY:
      if (buttonTimeUp.isPress()){
        t++;
        settingTime = t;
        delay(20);
        state = STATE_TIMEUP;
      }
      else if (buttonTimeDown.isPress()){
        t--;
        settingTime = t;
        delay(20);
        state = STATE_TIMEDOWN;
      }
      else if (buttonCount.isPress()){
        delay(20);
        state = STATE_STARTCOUNT;
      }
      break;

      
    case STATE_TIMEUP:
      if (!buttonTimeUp.isPress()){
        state = STATE_READY;
      }
      break;


    case STATE_TIMEDOWN:
      if (!buttonTimeDown.isPress()){
        state = STATE_READY;
      }
      break;


    case STATE_STARTCOUNT:
      v = 0;
      t = settingTime;
      state = STATE_COUNT;
      timerAlarmEnable(timer);
      attachInterrupt(digitalPinToInterrupt(pinCOUNTER), onCounter, FALLING);
      break;


    case STATE_COUNT:
      if (t < 0){
        detachInterrupt(digitalPinToInterrupt(pinCOUNTER));
        timerAlarmDisable(timer);
        state = STATE_READY;
      }
      break;
  }
}

void IRAM_ATTR onTimer(){
  t--;
}

void onCounter(){
  v++;
}

void timerSetting(){
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 1000000, true); 
}
