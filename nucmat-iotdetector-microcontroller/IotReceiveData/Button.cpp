#include "Button.h"

Button::Button(int pin){
  _pin = pin;
  pinMode(_pin, INPUT);
}

bool Button::isPress(){
  return !digitalRead(_pin);
}
