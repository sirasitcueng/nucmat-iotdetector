#ifndef Button_h
#define Button_h

#include "Arduino.h"

class Button{
  public:
    Button(int pin);
    bool isPress();
  private:
    int _pin;
  
};


#endif
