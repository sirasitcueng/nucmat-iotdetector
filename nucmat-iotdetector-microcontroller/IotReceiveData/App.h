#ifndef App_h
#define App_h

#include "Arduino.h"

typedef enum
{
  STATE_READY = 0,
  STATE_TIMEUP,
  STATE_TIMEDOWN,
  STATE_STARTCOUNT,
  STATE_COUNT,
  STATE_ENDCOUNT,
  STATE_REST,
}
STATE;

#define pinCOUNTER 27
#define pinTIMEUP 32
#define pinTIMEDOWN 35
#define pinBUZZERSW 33
#define pinBUZZER 25
#define pinCOUNT 2
#define pinCLK 32
#define pinDIN 33
#define pinCS 25




#endif
